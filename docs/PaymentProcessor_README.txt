################
# INSTALLATION #
################

1- Java version should be 1.6
2- Following environment variable should be set : 
	- SOISRV_DATABASE_SERVER
	- SOISRV_DATABASE_PORT
	- ORACLE_SID
3- Deploy & Unzip PaymentProcessor-1.0.0.zip
4- To run PaymentProcessor use : startPaymentProcessor.sh
5- Go to the http://<hostname>:19091

#############
# LOG LEVEL # 
#############

To increase log level from INFO to DEBUG, you can update LOG_LEVEL value in following files:
- startPaymentProcessor.sh




