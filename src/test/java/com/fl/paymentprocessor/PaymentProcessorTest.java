package com.fl.paymentprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

@Controller
@SpringBootApplication
public class PaymentProcessorTest {

    public static void main(String[] args) {
        SpringApplication.run(com.fl.paymentprocessor.PaymentProcessor.class, args);
    }
}
