package com.fl.paymentprocessor.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class AppConfig {

    private static String select_customer = "select distinct customer_id from SYSADM.ORDERHDR_ALL;";

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @PostConstruct
    void init() {

        List<Long> customerIdList = jdbcTemplate.queryForList(select_customer, Long.class);

        for (Long customerId : customerIdList) {

            String request = "insert into sysadm.customer_all(CUSTOMER_ID,CUSTCODE) values (:customerId, :custcode);";

            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("customerId", customerId);
            parameters.put("custcode", "CUST" + customerId.toString());

            NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
            namedParameterJdbcTemplate.update(request, parameters);
        }

        for (Long customerId : customerIdList) {

            String request = "insert into sysadm.ccontact_all(CUSTOMER_ID,CSCUSTTYPE, CCNAME, CCFNAME, CCLNAME, CCBILL ) " +
                    "values (:customerId, 'B', 'TEST', null, null, 'X');";

            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("customerId", customerId);

            NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
            namedParameterJdbcTemplate.update(request, parameters);
        }

    }

    @Bean
    public DataSource h2DataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("init-db-h2-schema.sql")
                .addScript("init-db-h2-orderhdr_all.sql")
                .addScript("init-db-h2-customer_all.sql")
                .addScript("init-db-h2-writeoff.sql")
                .build();
    }
}
