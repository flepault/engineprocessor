package com.fl.paymentprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@SpringBootApplication
@Profile("paymentprocessor")
public class PaymentProcessor implements ErrorController {

    public static void main(String[] args) {
        SpringApplication.run(PaymentProcessor.class, args);
    }

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error(){
        return "forward:/index.html";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
