package com.fl.paymentprocessor.endpoint;

import com.fl.paymentprocessor.entity.Customer;
import com.fl.paymentprocessor.service.ExtractPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PaymentProcessorEndpoint {

    @Autowired
    ExtractPaymentService extractPaymentService;

    @GetMapping(
            value = "/api/customer/export"
    )
    public @ResponseBody
    ResponseEntity<List<Customer>> getListClient(@RequestParam Long montant, @RequestParam String date) throws Exception {
        //ddmmyyyy
        return ResponseEntity.ok(this.extractPaymentService.extractCustomer(montant, date));
    }

    @PostMapping(
            value = "/api/customer/canceldebt"
    )
    public @ResponseBody
    ResponseEntity<List<Customer>> cancelDebt(@RequestBody List<Customer> customerList) throws Exception {
        this.extractPaymentService.cancelDebt(customerList);
        return ResponseEntity.ok(customerList);
    }

}
