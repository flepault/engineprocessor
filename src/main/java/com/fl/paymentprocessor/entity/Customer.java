package com.fl.paymentprocessor.entity;

public class Customer {

    Long customerId;

    String numeroClient;

    String noms;

    Float solde;

    Long ohxact;

    String numeroFacture;

    String dateFacture;

    String type;

    Float montantFacture;

    public Customer(){
    }

    public Customer(Long customerId, String numeroClient, String noms, Float solde, Long ohxact, String numeroFacture, String dateFacture, String type, Float montantFacture) {
        this.customerId = customerId;
        this.numeroClient = numeroClient;
        this.noms = noms;
        this.solde = solde;
        this.ohxact = ohxact;
        this.numeroFacture = numeroFacture;
        this.dateFacture = dateFacture;
        this.type = type;
        this.montantFacture = montantFacture;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getNumeroClient() {
        return numeroClient;
    }

    public void setNumeroClient(String numeroClient) {
        this.numeroClient = numeroClient;
    }

    public String getNoms() {
        return noms;
    }

    public void setNoms(String noms) {
        this.noms = noms;
    }

    public Float getSolde() {
        return solde;
    }

    public void setSolde(Float solde) {
        this.solde = solde;
    }

    public Long getOhxact() {
        return ohxact;
    }

    public void setOhxact(Long ohxact) {
        this.ohxact = ohxact;
    }

    public String getNumeroFacture() {
        return numeroFacture;
    }

    public void setNumeroFacture(String numeroFacture) {
        this.numeroFacture = numeroFacture;
    }

    public String getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(String dateFacture) {
        this.dateFacture = dateFacture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getMontantFacture() {
        return montantFacture;
    }

    public void setMontantFacture(Float montantFacture) {
        this.montantFacture = montantFacture;
    }

    @Override
    public String toString(){
        return customerId + "::" + ohxact + "::" + montantFacture + "::" + numeroFacture;
    }
}
