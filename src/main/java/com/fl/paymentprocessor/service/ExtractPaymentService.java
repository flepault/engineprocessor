package com.fl.paymentprocessor.service;

import com.fl.paymentprocessor.dao.PaymentProcessorDAO;
import com.fl.paymentprocessor.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("paymentprocessor")
public class ExtractPaymentService {

    @Autowired
    PaymentProcessorDAO paymentProcessorDAO;

    public List<Customer> extractCustomer(Long threhold, String date) throws Exception {

        return paymentProcessorDAO.extractCustomer(threhold, date);

    }

    public void cancelDebt(List<Customer> customers) {
        for (Customer customer : customers) {
            paymentProcessorDAO.cancelDebt(customer);
        }
    }
}
