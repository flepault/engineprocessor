package com.fl.paymentprocessor.dao;


/** .
 * @author efrelep
 *
 */
public class RequestTemplate {
	
	public static String EXTRACTION_WRITE_OFF =
            "SELECT " +
				"CU.CUSTOMER_ID as customerId, " +
				"CU.CUSTCODE AS numeroClient," +
				" CASE CC.CSCUSTTYPE WHEN 'B' THEN CC.CCNAME ELSE CC.CCFNAME||' '||CC.CCLNAME END AS noms,\n" +
				"A.SOLDE as solde, " +
				"B.OHXACT as ohxact, " +
				"B.OHREFNUM AS numeroFacture," +
				"TRUNC(B.OHENTDATE) AS dateFacture, B.OHSTATUS AS type, " +
				"B.OHOPNAMT_DOC AS montantFacture " +
            "FROM " +
            "(select oh.customer_id, sum(nvl(ohopnamt_doc,0)) as solde" +
            " from sysadm.orderhdr_all oh" +
            " where oh.ohstatus in ('IN','FC')" +
            " and nvl(oh.ohopnamt_doc,0) != 0" +
            " and oh.ohentdate < to_date(?,'ddmmyyyy')" +
            " and not exists (select 1 from sysadm.orderhdr_all where customer_id = oh.customer_id " +
            "                     and ohstatus in ('CM','CO') and ohopnamt_doc < 0) " +
            " group by oh.customer_id " +
            " having sum(nvl(ohopnamt_doc,0)) between 0 and ?) A," +
            "(select customer_id, ohxact, ohentdate, ohrefnum, ohstatus, ohopnamt_doc " +
            " from sysadm.orderhdr_all where ohstatus in ('IN','FC') " +
            " and ohentdate < to_date(?,'ddmmyyyy') " +
            " and nvl(ohopnamt_doc,0) != 0) B, " +
            " SYSADM.CUSTOMER_ALL CU, " +
            " SYSADM.CCONTACT_ALL CC " +
            "WHERE A.CUSTOMER_ID = CU.CUSTOMER_ID " +
            "AND A.CUSTOMER_ID = CC.CUSTOMER_ID " +
            "AND A.CUSTOMER_ID = B.CUSTOMER_ID " +
            "AND CC.CCBILL = 'X' " +
            "ORDER BY CU.CUSTOMER_ID, B.OHXACT";
		
}
