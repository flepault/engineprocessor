package com.fl.paymentprocessor.dao;

import com.fl.paymentprocessor.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class PaymentProcessorDAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    public List<Customer> extractCustomer(Long amountThreshold, String date) {

        return jdbcTemplate.query(RequestTemplate.EXTRACTION_WRITE_OFF,
                new Object[]{date, amountThreshold, date}
                , new BeanPropertyRowMapper<Customer>(Customer.class));
    }

    public void cancelDebt(Customer customer) {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("SPERTO")
                .withCatalogName("WRITEOFF")
                .withProcedureName("WRITEOFF")
                .withoutProcedureColumnMetaDataAccess();

        jdbcCall.addDeclaredParameter(new SqlParameter("pionCustomerId", Types.INTEGER));
        jdbcCall.addDeclaredParameter(new SqlParameter("pionOhxact", Types.INTEGER));
        jdbcCall.addDeclaredParameter(new SqlParameter("pionAmount", Types.FLOAT));
        jdbcCall.addDeclaredParameter(new SqlParameter("piosCachkNum", Types.VARCHAR));

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pionCustomerId", customer.getCustomerId());
        map.put("pionOhxact", customer.getOhxact());
        map.put("pionAmount", customer.getMontantFacture());
        map.put("piosCachkNum", "WR" + customer.getNumeroFacture());

        jdbcCall.execute(map);

    }

}
