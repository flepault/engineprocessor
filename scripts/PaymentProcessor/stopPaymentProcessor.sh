#!/bin/bash 
	
PP_PID=`ps -edf | grep $LOGNAME | grep "java" | grep "PaymentProcessor"| awk '{print $2}'`

echo "PaymentProcessor is stopping..."

kill -9 ${PP_PID}

sleep 5

echo "PaymentProcessor is stopped"
